/**
 * 
 * Have the function FirstReverse(str) take the str parameter being passed and
 * return the string in reversed order.
 * 
 * Example:
 *   "Hello World and Coders" --> "sredoC dna dlroW olleH"
 *
 */

function FirstReverse( str ) { 
    str = str.split( '' ).reverse().join( '' );
    return str; 
}

FirstReverse( "Hello World and Coders" );