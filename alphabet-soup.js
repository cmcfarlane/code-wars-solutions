/**
 * 
 * Have the function AlphabetSoup(str) take the str string parameter being passed and
 * return the string with the letters in alphabetical order.
 * Assume numbers and punctuation symbols will not be included in the string.
 * 
 * Example:
 *  'hello' becomes 'ehllo'
 */

function AlphabetSoup(str) { 
    str = str.split( '' ).sort().join( '' );
    return str; 
}

AlphabetSoup( 'hello' );