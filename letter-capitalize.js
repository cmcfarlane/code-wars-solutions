/**
 * 
 * Have the function LetterCapitalize(str) take the str parameter being
 * passed and capitalize the first letter of each word. Words will be
 * separated by only one space.
 * 
 * Sample Test Cases:
 *   LetterCapitalize( "hello world" ) // Hello World
 *   LetterCapitalize( "i ran there" ) // I Ran There
 * 
 */

function LetterCapitalize( str ) { 
    let split = str.split( ' ' );
    for ( let i = 0; i < split.length; i++ ) {
        split[i] = split[i].charAt(0).toUpperCase() + split[i].slice(1);
    }
    str = split.join( ' ' );

    console.log( str );

    return str; 
}
LetterCapitalize( "i ran there" );

function LetterCapitalizeRefactored( str ) { 
    str = ( str.split( /\s/ ) ).map(
            s => s.charAt(0).toUpperCase() + s.slice(1)
        ).join( ' ' );

    console.log( str );

    return str; 
}
LetterCapitalizeRefactored( "i ran there" );
