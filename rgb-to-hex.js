/**
 * The rgb() method is incomplete. Complete the method so that passing in RGB decimal values will result in a hexadecimal
 * representation being returned. The valid decimal values for RGB are 0 - 255. Any (r,g,b) argument values that fall out
 * of that range should be rounded to the closest valid value.
 *
 * The following are examples of expected output values:
 *   rgb(255, 255, 255) // returns FFFFFF
 *   rgb(255, 255, 300) // returns FFFFFF
 *   rgb(0,0,0) // returns 000000
 *   rgb(148, 0, 211) // returns 9400D3
 */

function rgb( r, g, b ) {
    let quotient     = 0;
    let remainder    = 0;
    let quotientHex  = 0;
    let remainderHex = 0;
    let hexCode      = '';
    let rgbArr       = [ r, g, b ];

    for ( let i = 0; i < rgbArr.length; i++ ) {
        quotient     = ( rgbArr[ i ] < 0 ) ? 0 :
                       ( ( rgbArr[ i ] > 255 ) ? 'f' : Math.floor( rgbArr[ i ] / 16 ) );
        remainder    = ( rgbArr[ i ] < 0 ) ? 0 :
                       ( ( rgbArr[ i ] > 255 ) ? 'f' : Math.floor( rgbArr[ i ] % 16 ) );
        quotientHex  = ( quotient > 9 ) ?
                       String.fromCharCode(97 + ( quotient - 10 ) ) :
                       quotient.toString();
        remainderHex = ( remainder > 9 ) ?
                       String.fromCharCode(97 + ( remainder - 10 ) ) :
                       remainder.toString();

        hexCode += ( quotientHex + remainderHex );
    }

    return hexCode.toUpperCase();
}
console.log( rgb( 16, 208, 255 ) );

function rgbSimplified( r, g, b ) {
    let hexCode = '';
    let rgbArr = [ r, g, b ];

    for ( let i = 0; i < rgbArr.length; i++ ) {
        hexCode += ( rgbArr[ i ] <= 0 ) ? '00' :
                   ( ( rgbArr[ i ] > 255 ) ? 'ff' : rgbArr[ i ].toString( 16 ) );
    }

    return hexCode.toUpperCase();
}
console.log( rgbSimplified( 16, 208, 255 ) );
console.log( rgbSimplified( 255, 255, 300 ) );
console.log( rgbSimplified(0,0,0) );// returns 000000
console.log( rgbSimplified(148, 0, 211) );// returns 9400D3

